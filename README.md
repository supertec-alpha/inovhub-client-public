# o2 HUB

## Client

Application gérant la diffusion des playlists via le support _Inovbox_ pour la plateforme de L'Oréal Professionnel : o2 HUB

Executez `npm start` dans la console pour installer toutes les dépendances et librairies.

When this repo is updated, the docker image inovhub-box is built with the newest changes.