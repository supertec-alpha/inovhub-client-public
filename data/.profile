
# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

#fbi /usr/share/plymouth/themes/pix/splash-270.png --noverbose 2>/dev/null

sudo service ssh restart

# Run InovBox update script if Internet connection
wget -q --spider http://google.com >/dev/null 2>/dev/null
if [ $? -eq 0 ]; then
    cd /opt/inovbox-alpha-updater/
    sudo ./update.sh
fi

unclutter -idle 0.01 -root &

cd /opt/inovhub-client-public/
sudo pm2 start app.js

#Wait for Node.JS server to start
#displaySplash
while ! pgrep -f node >/dev/null 2>/dev/null
do
    sleep 1
done

#chromium-browser --noerrdialogs --start-fullscreen --disable-session-crashed-bubble --kiosk --disable-infobars --incognito --suppress-message-center-popups --window-size=1920,1080 --window-position=0,0 http://localhost:3000
chromium-browser --noerrdialogs --start-fullscreen --disable-session-crashed-bubble --kiosk --disable-infobars --incognito --suppress-message-center-popups --disable-component-update --check-for-update-interval=604800 --window-size=1920,1080 --window-position=0,0 http://localhost:3000
