'use strict';

var gulp            = require('gulp');

var sass            = require('gulp-sass');
var eslint          = require('gulp-eslint');
var browserSync     = require('browser-sync').create();

gulp.task('sass', function() {
    return gulp.src([
        'www/scss/**/*.scss',
        '!www/scss/**/_*.scss'
    ])
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('www/css'))
        .pipe(browserSync.stream());
});

gulp.task('eslint', function() {
    return gulp.src([
        'app.js',
        'modules/**/*.js',
        'www/**/*.js'
    ])
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});

gulp.task('watch', ['sass', 'eslint'], function() {
    gulp.watch('www/scss/**/*.scss', ['sass']);
    gulp.watch([
        'app.js',
        'modules/**/*.js',
        'www/**/*.js'
    ], ['eslint']);
});

gulp.task('serve', ['sass', 'eslint'], function() {

    // Static server
    browserSync.init(null, {
		proxy: "http://localhost:3000",
        files: [
            "**/*.js",
            "**/*.scss",
            "**/*.html",
            "**/*.json"
        ],
        browser: "google chrome",
        port: 7000,
	});

    gulp.watch('www/scss/**/*.scss', ['sass']);
    gulp.watch([
        'app.js',
        'modules/**/*.js',
        'www/**/*.js'
    ], ['eslint']);
    gulp.watch('www/scss/**/*.html', browserSync.reload);
});