'use strict';

const os = require('os');

const file = require('./file');

class Config {

    constructor() {
        this._platform = os.platform();
        this._portal = process.env.PORTAL;
        this._server = '';
    }

    read() {
        let that = this;
        return file.readJSON('./data/config.json').then(function(data) {
            if (data.portal) {
                that._portal = data.portal;
            }
            if (data.server) {
                that._server = data.server;
            }
            return data;
        });
    }

    save(data) {
        let that = this;
        if (data.portal) {
            that._portal = data.portal;
        }
        if (data.server) {
            that._server = data.server;
        }
        return file.saveJSON('./data/config.json', { "portal": that._portal, "server": that._server });
    }

    get platform() {
        return this._platform;
    }

    get portal() {
        return this._portal;
    }

    set portal(value) {
        this._portal = value;
    }

    get server() {
        return this._server;
    }

    set server(value) {
        this._server = value;
    }

}

module.exports = Config;