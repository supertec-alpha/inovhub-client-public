const https = require('https');
const fs = require('fs');
const Throttle = require('throttle');
const log = require('./log');
require('dotenv').config();

class DownloadManager {

    constructor() {
        this._speed;
        this._nbFiles = 0;
        this._nbDownloadedFiles = 0;
        this._totalSizeDownloaded = 0;
        this._totalSize = 0;
        this._files = [];
        this._throttleSpeed = 0;
    }

    /**
     * Télécharge un fichier dans le répertoire de destination (avec vérification de présence)
     * @param {Object} file Fichier à télécharger (filename, size, file_path)
     * @param {string} path Chemin de destination
     * @param {function} progress Callback contenant l'état du téléchargement (loaded, total, percent)
     */
    _downloadFile(url, path, progress) {
        let that = this;

        // Retourne une promise
        return new Promise(function(resolve, reject) {

            // On teste si le fichier existe ...
            fs.stat(path + url.filename, function(err, stats) {

                // Si le fichier n'existe pas ou que sa taille est différente, on autorise le téléchargement...
                if (typeof stats === "undefined" || stats.size != url.size) {

                    // On lance le téléchargement du fichier
                    let request = https.get(url.file_path, function(response) {
                        // Calcul du temps de dl
                        var startTime = new Date().getTime();
                        var elapsedTime = 0;

                        // Si le serveur retourne une erreur, on rejette la promise
                        if ([403, 404, 500].includes(response.statusCode)) reject(response.statusCode);

                        // On définit la variable contenant les octets téléchargés
                        let loaded = 0;

                        // On définit la variable contenant la taille totale du fichier
                        //let total = parseInt(response.headers['content-length'], 10);
                        let total = url.size;

                        // Fichier de destination
                        let file = fs.createWriteStream(path + url.filename);
                        // Vitesse de téléchargement
                        that._throttleSpeed = parseInt((that._speed * 0.9), 10);
                        // Ajout du fichier "objet" à la liste
                        let currentFile = {
                            filename: url.filename,
                            loadedSize: loaded,
                            totalSize: total,
                            percentLoaded: Math.round((loaded / total) * 100),
                        }
                        that._files.push(currentFile);

                        if (typeof that._speed !== "undefined" && !isNaN(that._speed)) {
                            //Création du "limiteur de download"
                            let throttleSpeed = parseInt((that._speed * 0.9), 10);
                            /*if (throttleSpeed > 2000000) {
                                throttleSpeed = 2000000;
                            }*/
                            var throttle = new Throttle(throttleSpeed);
                            response.pipe(throttle).pipe(file);
                        } else {
                            response.pipe(file);
                        }

                        response.on('data', (chunk) => {
                            elapsedTime = new Date().getTime() - startTime;
                            // Calcul de la taille chargée + remplissage de l'objet file
                            loaded += chunk.length;
                            that._totalSizeDownloaded += chunk.length;
                            currentFile.loadedSize = loaded;
                            // Si la taille chargée est égale au total, on incrémente le nombre de dl
                            if (loaded == total) {
                                that._nbDownloadedFiles++;
                            }
                            loadProgress(elapsedTime);
                        });

                        // Event de fin de téléchargement
                        file.on('finish', function() {
                            file.close(function() {
                                log.debug('Download complete: ' + url.filename);
                                resolve(file);
                            });
                        });

                        // Event en cas d'erreur
                        request.on("error", function(err) {
                            reject(err);
                        });

                        // En cas d'erreur
                    }).on('error', function(err) {
                        reject(err);
                    });

                } else {
                    // Le fichier existe et est identique, on ne le retélécharge pas
                    // Mise à jour de progress
                    that._nbDownloadedFiles++;
                    that._totalSizeDownloaded += url.size;
                    loadProgress(0);
                    resolve(url);

                }

            });
        });

        function loadProgress(elapsedTime) {
            progress({
                files: that._files,
                nbDownloadedFiles: that._nbDownloadedFiles,
                nbFiles: that._nbFiles,
                totalSize: that._totalSize,
                totalSizeDownloaded: that._totalSizeDownloaded,
                time: elapsedTime,
                speed: that._throttleSpeed
            });
        }
    }

    /**
     * Télécharge une liste de fichiers dans le répertoire de destination (avec vérification de présence)
     * @param {Object[]} file Fichier à télécharger (filename, size, file_path)
     * @param {string} path Chemin de destination
     * @param {function} progress Callback contenant l'état du téléchargement (loaded, total, percent)
     */
    _downloadFiles(files, path, progress, currentFileIndex) {
        this._totalSize = files.reduce(function(a, b) {
            return a + (b.size || 0);
        }, 0);

        let that = this;

        return that._downloadFile(files[currentFileIndex], path, progress).then(function() {
            if (currentFileIndex < files.length - 1) {
                return that._downloadFiles(files, path, progress, currentFileIndex + 1);
            }
        });
    }

    /**
     * Télécharge un fichier ou plusieurs dans le répertoire de destination (avec vérification de présence)
     * @param {Object[]|Object} file Fichier ou tableau de fichier à télécharger ([filename, size, file_path])
     * @param {string} path Chemin de destination
     * @param {function} progress Callback contenant l'état du téléchargement (loaded, total, percent)
     */
    download(files, path, progress, speedFile) {
        let that = this;
        that._nbDownloadedFiles = 0;
        that._totalSizeDownloaded = 0;
        that._files = [];
        return new Promise((resolve, reject) => {
            this._speedtest(speedFile, path)
                .then((result) => {
                    that._speed = result.speed;
                })
                .catch((err) => {
                    console.log("Impossible de tester la connexion:", err);
                })
                .then(() => {
                    if (Array.isArray(files)) {
                        that._nbFiles = files.length;
                        that._downloadFiles(files, path, progress, 0)
                            .then(() => {
                                resolve();
                            })
                            .catch(() => {
                                reject();
                            });
                    } else {
                        that._nbFiles = 1;
                        return that._downloadFile(files, path, progress)
                            .then(() => {
                                resolve();
                            })
                            .catch(() => {
                                reject();
                            });
                    }
                });
        });
    }

    /**
     * Télécharge un fichier depuis le serveur pour calculer la vitesse de téléchargement
     * @param {Object[]|Object} file Fichier ou tableau de fichier à télécharger ([filename, size, file_path])
     * @param {string} path Chemin de destination
     */
    _speedtest(speedFile, destinationPath) {
        return new Promise((resolve, reject) => {
            let request = https.get(speedFile, function(response) {
                // Calcul du temps de dl
                var startTime = new Date().getTime();
                var endTime, duration, speed;

                // Si le serveur retourne une erreur, on rejette la promise
                if ([403, 404, 500].includes(response.statusCode)) reject(response.statusCode);

                // On définit la variable contenant la taille totale du fichier
                let fileSize = parseInt(response.headers['content-length'], 10);

                // Fichier de destination
                let file = fs.createWriteStream(`${destinationPath}speed.test`);
                response.pipe(file);

                // Event de fin de téléchargement
                file.on('finish', function() {
                    endTime = new Date().getTime();
                    duration = (endTime - startTime) / 1000;
                    speed = fileSize / duration;
                    file.close(function() {
                        resolve({
                            speed: speed
                        });
                    });
                });

                // Event en cas d'erreur
                request.on("error", function(err) {
                    reject(err);
                });

                // En cas d'erreur
            }).on('error', function(err) {
                reject(err);
            });
        });
    };


}

module.exports = DownloadManager;