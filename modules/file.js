const fs = require('fs');

function readJSON(file) {
    return new Promise(function(resolve, reject) {
        fs.readFile(file, function(err, data) {
            if (!err) {
                if (data.length > 0) {
                    resolve(JSON.parse(data));
                } else {
                    reject();
                }
            } else {
                reject(err);
            }
        });
    });
};

function saveJSON(file, data) {
    return new Promise(function(resolve, reject) {
        fs.writeFile(file, JSON.stringify(data, null, 4), 'utf8', function(err) {
            if (err) reject(err);
            resolve(true);
        });
    });
};

function saveFile(file, data) {
    return new Promise(function(resolve, reject) {
        fs.writeFile(file, data, 'utf8', function(err) {
            if (err) reject(err);
            resolve();
        });
    });
}

module.exports = {
    readJSON: readJSON,
    saveJSON: saveJSON,
    saveFile: saveFile
};