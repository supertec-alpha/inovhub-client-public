const util = require('util');
const chalk = require('chalk');

const types = {
    debug: chalk.grey("DEBUG: "),
    success: chalk.green("SUCCESS: "),
    info: chalk.cyan("INFO: "),
    warn: chalk.yellow("WARN: "),
    error: chalk.red("ERROR: ")
};

function getTimestamp() {
    return chalk.grey('[' + new Date().toLocaleTimeString() + '] ');
};

function log(type, value) {
    console.log(getTimestamp() + type + ((typeof value === 'object') ? util.inspect(value, { colors: true }) : value));
};

module.exports = {
    debug: function(value) {
        log(types.debug, value);
    },

    success: function(value) {
        log(types.success, value);
    },

    info: function(value) {
        log(types.info, value);
    },

    warn: function(value) {
        log(types.warn, value);
    },

    error: function(value) {
        log(types.error, value);
    },

    star: function(value) {
        console.log(getTimestamp() + chalk.black.bgRed(" ★ " + ((typeof value === 'object') ? util.inspect(value, { colors: true }) : value) + " "));
    }
};