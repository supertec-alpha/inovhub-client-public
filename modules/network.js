'use strict';

const os = require('os');
const macaddress = require('macaddress');
const dns = require('dns');
const { exec } = require('child_process');
const internetAvailable = require('internet-available');

class Network {
  getMacAddress() {
    return new Promise(function(resolve, reject) {
      if (os.platform() === 'win32') {
        macaddress.one(function(err, mac) {
          if (err) reject(err);
          resolve(mac);
        });
      } else {
        exec('cat /sys/class/net/eth0/address', function(err, stdout, stderr) {
          if (err) reject(err);
          resolve(stdout.toUpperCase().trim());
        });
      }
    });
  }

  checkConnection() {
    return new Promise(async (resolve, reject) => {
      try {
        await internetAvailable({
          timeout: 10000,
          retries: 13
        });
        resolve({ connection: true, error: null });
      } catch (errTest) {
        reject({ connection: false, errTest });
      }
    });
  }
}

module.exports = Network;
