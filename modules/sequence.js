'use strict';

const file = require('./file');
const remoteFileSize = require('remote-file-size');

class Sequence {

    constructor() {
        this._name = null;
        this._slots = [];
    }

    read() {
        let that = this;
        return file.readJSON('./data/sequence.json').then(function(data) {
            that._slots = data.slots;
            return data;
        });
    }

    save(data) {
        let that = this;
        that._name = data.name;
        that._slots = data.slots;
        return file.saveJSON('./data/sequence.json', data);
    }

    getAssets() {
        return new Promise((resolve, reject) => {
            let assets = [];

            let inovgenPromises = [];

            this._slots.forEach(function(slot) {
                slot.template.template_fields.forEach(function(templateField) {
                    if (templateField.filename) {
                        assets.push({
                            filename: templateField.filename,
                            file_path: templateField.file_path,
                            size: templateField.filesize
                        });
                    } else if (templateField.slug == '__render' && templateField.value) {
                        inovgenPromises.push(new Promise((resolve, reject) => {
                            remoteFileSize(templateField.value, (err, remoteSize) => {
                                if (err) {
                                    reject(err);
                                    console.log(err)
                                } else {
                                    let asset = {
                                        filename: templateField.value.slice(templateField.value.lastIndexOf('/') + 1, templateField.value.lastIndexOf('?decache')),
                                        file_path: templateField.value,
                                        size: remoteSize
                                    }
                                    assets.push(asset);
                                    resolve(asset);
                                }
                            });
                        }));
                    }
                });
            });

            Promise.all(inovgenPromises).then(() => {
                resolve(assets);
            }).catch((err) => { reject(err) });

        })
    }

    get slots() {
        return this._slots;
    }

    set slots(value) {
        this._slots = value;
    }

}

module.exports = Sequence;