'use strict';

const si = require('systeminformation');

class System {

    static async getCpu() {
        try {
            let data = await si.cpu();
            return data;
        } catch (e) {
            return e;
        }
    }

    static async getCpuLoad() {
        try {
            let data = await si.currentLoad();
            return data;
        } catch (e) {
            return e;
        }
    }

    static async getCpuTemperature() {
        try {
            let data = await si.cpuTemperature();
            return data;
        } catch (e) {
            return e;
        }
    }

    static async getProcesses() {
        try {
            let data = await si.processes();
            return data;
        } catch (e) {
            return e;
        }
    }

    static async getProcessLoad(name) {
        try {
            let data = await si.processLoad(name);
            return data;
        } catch (e) {
            return e;
        }
    }

    static async getMemory() {
        try {
            let data = await si.mem();
            return data;
        } catch (e) {
            return e;
        }
    }

    static async getFileSystemSize() {
        try {
            let data = await si.fsSize();
            return data;
        } catch (e) {
            return e;
        }
    }

    static async getFileSystemStats() {
        try {
            let data = await si.fsStats();
            return data;
        } catch (e) {
            return e;
        }
    }

    static async getNetworkInterfaces() {
        try {
            let data = await si.networkInterfaces();
            return data;
        } catch (e) {
            return e;
        }
    }

    static async getNetworkStats(iface) {
        try {
            let data = await si.networkStats(iface);
            return data;
        } catch (e) {
            return e;
        }
    }

    static getTime() {
        return si.time();
    }

}

module.exports = System;