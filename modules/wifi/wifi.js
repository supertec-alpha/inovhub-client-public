'use strict';
const { exec, execSync } = require('child_process');
const express = require('express');
const app = express();
const http = require('http').Server(app);
const iw = require('wireless-tools/iw');
const hostapd = require('wireless-tools/hostapd');
const bodyParser = require('body-parser');
const EventEmitter = require('events').EventEmitter;
const crypto = require('crypto');
const hash = crypto.createHash('sha256');
const file = require('../file');
const pug = require('pug');
const utf8 = require('utf8');
const internetAvailable = require('internet-available');

const port = '80',
  ip = '10.10.10.10',
  netmask = '255.255.255.192',
  broadcast = '10.10.10.63';
var salt = 'DevForceOne';

class wifi extends EventEmitter {
  //Middleware Gestion POST json
  constructor() {
    super();
    this.launch();
    this._enabled = false;
    this._mac = '';
    this._ssid = '';
    this._passphrase = '';
  }

  // Lance l'app
  launch() {
    var self = this;
    exec(
      'cat /sys/class/net/eth0/address',
      { shell: '/bin/bash' },
      (errMac, mac) => {
        if (!errMac) {
          self._mac = mac.toUpperCase().trim();
          hash.update(self._mac + salt);
          var hashMac = hash.digest('hex');
          // Options Hotspot
          self._ssid = 'Inovbox-' + hashMac.substr(0, 10);
          self._passphrase = hashMac.substr(54, 10);
        } else {
          console.log(errMac);
        }
      }
    );

    app.use(bodyParser.json());

    //Middleware Gestion POST url
    app.use(
      bodyParser.urlencoded({
        extended: true
      })
    );

    async function checkConnection() {
      return new Promise(async (resolve, reject) => {
        try {
          await internetAvailable({
            timeout: 10000,
            retries: 13
          });
          self.emit('wifi:connected', true);
          resolve({ connection: true, error: null });
        } catch (errTest) {
          self.emit('wifi:disconnected', true);
          self.enable();
          reject({ connection: false, errTest });
        }
      });
    }

    //Configuraton Pug
    app.use('/js', express.static(__dirname + '/views/js'));
    app.use('/css', express.static(__dirname + '/views/css'));
    app.use('/fonts', express.static(__dirname + '/views/fonts'));
    app.set('views', __dirname + '/views');
    app.set('view engine', 'pug');
    app.get('views');

    app.get('/', (req, res) => {
      iw.scan('wlan0', (err, networks) => {
        if (networks == undefined) {
          iw.scan('wlan0', (err, networks) => {
            if (err === null) {
              // Pour chaque réseaux wifi, on traduit le signal en "qualité"
              networks.forEach(network => {
                network.quality = self.rssi2Quality(network.signal);
                network.ssid = utf8.decode(self.utf8Decode(network.ssid));
              });
              // Chargement du html
              res.render('index', {
                title: 'Inovbox Wifi',
                networks: networks
              });
            } else {
              console.error(err);
            }
          });
        } else {
          iw.scan('wlan0', (err, networks) => {
            if (err === null) {
              // Pour chaque réseaux wifi, on traduit le signal en "qualité"
              networks.forEach(network => {
                network.quality = self.rssi2Quality(network.signal);
                network.ssid = utf8.decode(self.utf8Decode(network.ssid));
              });
              // Chargement du html
              res.render('index', {
                title: 'Inovbox Wifi',
                networks: networks
              });
            } else {
              console.error(err);
            }
          });
        }
      });
    });

    // Receive ssid et ask for wpa
    app.post('/sendSSID', (req, res) => {
      if (typeof req.body.ssid !== 'undefined') {
        res.render('sendSSID', {
          title: 'Inovbox Wifi',
          ssid: req.body.ssid
        });
      } else {
        console.log({ error: 'Parameter id not given' });
        res.status(400).send({ error: 'Parameter id not given' });
      }
    });

    // Send wpa and ssid
    app.post('/sendWifi', async (req, res) => {
      console.log(req.body);
      if (
        typeof req.body.ssid !== 'undefined' &&
        typeof req.body.wpa !== 'undefined'
      ) {
        // On écrit le fichier de conf wifi pour tester la connexion
        if (req.body.wpa == '') {
          var dataWifi = `ctrl_interface=/run/wpa_supplicant\nupdate_config=1\nnetwork={\n    ssid="${req.body.ssid}"\n    key_mgmt=NONE\n}`;
        } else {
          var dataWifi = `ctrl_interface=/run/wpa_supplicant\nupdate_config=1\nnetwork={\n    ssid="${req.body.ssid}"\n    psk="${req.body.wpa}"\n}`;
        }
        self.emit('wifi:connecting', true);
        file.saveFile(
          '/etc/wpa_supplicant/wpa_supplicant-wlan0.conf',
          dataWifi
        );
        // on réactive le wifi et on désactive le hotspot et on check la connection
        try {
          await self.disable();
          await checkConnection();
        } catch (err) {
          console.log('Unable to write wifi conf', err);
        }
      } else {
        res.redirect('/');
        console.log({ error: 'Parameter id not given' });
      }
    });
  }

  /**
   * Active le Réseau Wifi de la box
   *  - scan des réseaux
   *  - désactiver wpa_supplicant (systemctl stop wpa_supplicant@wlan0)
   *  - Active l'ip
   *      - sudo ifconfig wlan0 up 10.20.30.41 netmask 255.255.255.252 broadcast 10.20.30.43
   *      - sudo dnsmasq
   *  - TODO supprimer le passphrasewpa (open wifi)
   *  - interface html/js pour submit le ssid
   */
  enable() {
    var self = this;
    this._enabled = true;
    return new Promise(function(resolve, reject) {
      // On désactive la connexion auto au wifi (wpa_supplicant)
      // On active l'ip du hotspot (ifconfig & dnsmasq)
      file
        .saveFile(
          '/etc/dnsmasq.conf',
          'interface=wlan0\nlisten-address=10.10.10.10\nbind-interfaces\nserver=8.8.8.8\ndomain-needed\nbogus-priv\nno-resolv\ndhcp-range=10.10.10.11,10.10.10.62,1h\n'
        )
        .then(() => {
          console.log('Config DHCP sauvegardée');
        })
        .catch(err => {
          console.log(err);
        })
        .then(() => {
          exec(
            `systemctl stop wpa_supplicant@wlan0 && ifconfig wlan0 up ${ip} netmask ${netmask} broadcast ${broadcast} && dnsmasq`,
            { shell: '/bin/bash' },
            errIp => {
              if (!errIp) {
                // On active le hotspot
                var hostapdOptions = {
                  channel: 6,
                  driver: 'nl80211',
                  hw_mode: 'g',
                  interface: 'wlan0',
                  ssid: self._ssid,
                  wpa: 2,
                  wpa_passphrase: self._passphrase
                };
                hostapd.enable(hostapdOptions, errHostapd => {
                  if (errHostapd == null) {
                    http.close();
                    // IP et Hotspot activés, on écoute sur le port de notre choix
                    http.listen(port, ip, 511, (req, res) => {
                      console.log(`Listening on *: ${port}`);
                      resolve({
                        ssid: self._ssid,
                        passphrase: self._passphrase
                      });
                    });
                  } else {
                    reject(errHostapd);
                  }
                });
              } else {
                reject(errIp);
              }
            }
          );
        });
    });
  }

  /**
   * Désactive le réseau Wifi de la box
   * - inverser
   *      - sudo ifconfig wlan0 up 10.20.30.41 netmask 255.255.255.252 broadcast 10.20.30.43
   *      - sudo dnsmasq
   * - activer wpa_supplicant
   * - se connecter au wifi
   */
  disable() {
    return new Promise((resolve, reject) => {
      // On kill les 2 processus
      exec('killall dnsmasq hostapd', { shell: '/bin/bash' }, () => {
        try {
          execSync(
            'ifconfig wlan0 down && ifconfig wlan0 up && systemctl start wpa_supplicant@wlan0 && systemctl enable wpa_supplicant@wlan0',
            { shell: '/bin/bash' }
          );
          resolve();
        } catch (err) {
          console.log(err.status, err.message, err.stderr, err.stdout);
          reject(err);
        }
        resolve();
      });
    });
  }

  utf8Decode(str) {
    str = str.replace(/\\x[\dA-F]{2}/gi, function(match) {
      return String.fromCharCode(parseInt(match.replace(/\\x/g, ''), 16));
    });
    return str;
  }

  rssi2Quality(dBm) {
    if (!Number.isInteger(dBm)) return 'Unusable';
    if (dBm > -67) {
      return 'Excellent';
    } else if (dBm > -70) {
      return 'Very good';
    } else if (dBm > -80) {
      return 'Okay';
    } else if (dBm > -90) {
      return 'Not good';
    } else {
      return 'Unusable';
    }
  }

  get enabled() {
    return this._enabled;
  }

  set enabled(value) {
    this._enabled = parseInt(value);
  }
}

module.exports = wifi;
