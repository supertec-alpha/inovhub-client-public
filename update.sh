#!/usr/bin

boxname=`uname -n`

if [ "$boxname" = "raspberrypi" ]; then
    sudo cp data/.profile /home/pi/
    sudo rm -f /opt/inovhub-client-public/update.sh
fi