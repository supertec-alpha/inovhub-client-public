/*global angular*/

(function() {
  'use strict';

  angular
    .module('app')
    .config(config)
    .run(run);

  config.$inject = ['$urlRouterProvider'];
  run.$inject = ['$transitions', 'SocketFactory'];

  function config($urlRouterProvider) {
    // Route par défaut
    $urlRouterProvider.otherwise('/');
  }

  function run($transitions, SocketFactory) {
    $transitions.onStart({}, function(trans) {
      SocketFactory.emit('page:change', trans.to().name);
    });
  }
})();
