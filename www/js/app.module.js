/*global angular*/

(function() {
    'use strict';

    angular.module('app', [
        // angular modules
        'ngAnimate',
        'ngSanitize',
        // 3rd party modules
        'ui.router',
        // app modules
        'app.socket',
        'app.core',
        'app.directives',
        'app.main',
        'app.loading',
        'app.empty',
        'app.start',
        'app.wifi',
        'app.error',
        'app.register',
        'app.sequence'
    ]);
})();