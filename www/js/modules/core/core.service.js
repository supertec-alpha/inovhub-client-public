(function() {
    'use strict';

    angular
        .module('app.core')
        .factory('CoreService', Core)

    Core.$inject = [];

    function Core() {
        var that = this;

        that.platform = "linux";
        that.mac = null;
        that.settings = null;
        that.progress = null;
        that.debug = null;
        that.slots = [];
        that.systemInfos = {};

        var service = {
            getPlatform: getPlatform,
            setPlatform: setPlatform,
            getMac: getMac,
            setMac: setMac,
            getSettings,
            setSettings,
            getProgress,
            setProgress,
            getDebug,
            setDebug,
            getSlots,
            setSlots,
            getSystemInfos,
            setSystemInfos
        };

        return service;

        function getPlatform() {
            return that.platform;
        }

        function setPlatform(platform) {
            that.platform = platform;
        }

        function getMac() {
            return that.mac;
        }

        function setMac(mac) {
            that.mac = mac;
        }

        function getSettings() {
            return that.settings;
        }

        function setSettings(settings) {
            that.settings = settings;
        }

        function getProgress() {
            return that.progress;
        }

        function setProgress(progress) {
            that.progress = progress;
        }

        function getDebug() {
            return that.debug;
        }

        function setDebug(debug) {
            that.debug = debug;
        }

        function getSlots() {
            return that.slots;
        }

        function setSlots(slots) {
            that.slots = slots;
        }

        function getSystemInfos() {
            return that.systemInfos;
        }

        function setSystemInfos(infos) {
            that.systemInfos = infos;
        }
    }
})();