/*global angular*/

(function () {
    'use strict';

    angular
        .module('app.directives')
        .directive('remoteContent', remoteContent);

    remoteContent.$inject = ['$compile', '$sce', '$http'];

    function remoteContent($compile, $sce, $http) {
        var directive = {
            transclude: true,
            restrict: 'A',
            scope: {
                remoteContent: '='
            },
            controller: controller,
            link: link
        };
        return directive;

        function controller($scope, $sce, $http) {

            var contentType = $scope.remoteContent.contentType;
            var opts = $scope.remoteContent.opts;
            
            //var slot = $scope.remoteContent.slot;
            console.log($scope.$parent.slot);

            var reqUrl = "https://marvin.inovhub.com/" + contentType + "?";
            for (var i = 0; i < opts.length; ++i) {
                console.log(opts[i]);
                reqUrl += opts[i] + "=" + $scope.$parent.getBySlug(opts[i]) + "&"
            }
            reqUrl = reqUrl.slice(0, -1);
            console.log("making request for " + reqUrl);
            $http.get($sce.trustAsResourceUrl(reqUrl)).then((response) => {
                console.log(response);
                $scope.remoteContent = response;
            }).catch((err) => {
                //console.log(err);
            });

            // refresh data...
            window.setInterval(function() {
                $http.get($sce.trustAsResourceUrl(reqUrl)).then((response) => {
                    //console.log(response);
                    $scope.remoteContent = response;
                }).catch((err) => {
                    //console.log(err);
                });
            }, 1000*60*60*3);  // ...once every three hours (which is the refresh time of OpenWeatherMap API)
        }

        function link(scope, elem) {
            //console.log(scope);

            scope.$watch('remoteContent', function () {
                //console.log("$watch triggered - scope is:");
                //console.log(scope);
                elem.html('');
                if (scope.remoteContent) {
                    // on l'attache a l'element
                    elem.append(scope.remoteContent.data);
                }
            });
        }
    }

})();