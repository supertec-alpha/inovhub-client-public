/*global angular*/

(function () {
    'use strict';

    angular
        .module('app.directives')
        .directive('renderSlot', renderSlot);

    renderSlot.$inject = ['$compile', '$sce'];

    function renderSlot($compile) {
        var directive = {
            restrict: 'E',
            scope: {
                slot: '=',
                progress: '=',
                debug: '=',
                settings: '='
            },
            controller: controller,
            link: link
        };
        return directive;

        function controller($scope, $sce, $http) {
            $scope.getBySlug = function (slug) {
                var v = null;
                if (typeof $scope.slot !== "undefined" && typeof $scope.slot.template !== "undefined") {
                    $scope.slot.template.template_fields.forEach(function (field) {
                        if (field.slug == slug) {
                            if (slug == "post") {
                                v = $sce.trustAsHtml(field.value);   // pretty lax/unsafe? - trusts anything and everything that's entered into embed template field
                            } else if (slug == "url") {
                                v = $sce.trustAsResourceUrl(field.value);
                            } else if(slug == '__render'){
                                v = 'assets' + field.value.slice(field.value.lastIndexOf('/'), field.value.lastIndexOf('?decache'));
                            } else {
                                v = field.value || 'assets/' + field.filename;
                            }
                        }
                    });
                }
                return v;
            }

            $scope.getRemoteContent = function(type, parameters) {

                console.log(type);
                console.log(parameters);

                return true;

                /*var reqUrl = "http://192.168.1.110:3000/" + type + "?";  // TODO: change address
                var params = parameters;
                for (var k in params) {
                    var v = null;
                    $scope.slot.template.template_fields.forEach(function (field) {
                        if (field.slug = k) {
                            v = field.value;
                        }
                    });
                    reqUrl += k + "=" + encodeURIComponent(v) + "&";
                }
                reqUrl = reqUrl.slice(0, -1);
                var content = null;
                $http.get(reqUrl).then((response) => {
                    if ($scope.contentType != type) {
                        $scope.contentType = type;
                        $scope.content = $sce.trustAsHtml(response);
                    }
                    //$scope.contentType === type ? console.log("nothing changed") : $scope.content = $sce.trustAsHtml(response);
                    //return response;
                });
                //console.log("ici");
                //return 'ça';

                /*
                var p = new Promise(() => {
                    $http.get(reqUrl).then((response) => {
                        content = $sce.trustAsHtml(response);
                    });
                });

                p.then(() => { return content; });
                */
            }
        }

        function link(scope, elem) {
            scope.$watch('slot', function () {
                elem.html('');
                if (scope.slot.layout) {
                    // template fourni par le serveur
                    var compiledTemplate = $compile(scope.slot.layout.html)(scope);

                    // on l'attache a l'element
                    elem.append(compiledTemplate);

                    // on supprime les anciens styles liés aux templates
                    angular.element('#layout-' + scope.slot.layout.id).remove();

                    // on créé l'element style
                    var css = angular.element('<style id="layout-' + scope.slot.layout.id + '">' + scope.slot.layout.css + '</style>');

                    // on append l'element
                    angular.element(document.head).append(css);
                }
            });
        }
    }

})();