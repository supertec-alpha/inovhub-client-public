/*global angular*/

(function() {
    'use strict';

    angular
        .module('app.empty')
        .controller('EmptyController', Empty)

    Empty.$inject = ['$scope'];

    function Empty($scope) {
        //var vm = this;

        activate();

        function activate() {}
    }
})();