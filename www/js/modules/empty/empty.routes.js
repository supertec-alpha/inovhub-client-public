(function() {
    'use strict';

    angular
        .module('app.empty')
        .config(configRoutes);

    configRoutes.$inject = ['$stateProvider'];

    function configRoutes($stateProvider) {
        $stateProvider
            .state('empty', {
                url: '/empty',
                templateUrl: 'js/modules/empty/empty.tmpl.html',
                controller: 'EmptyController',
                controllerAs: 'vm'
            });
    }

})();