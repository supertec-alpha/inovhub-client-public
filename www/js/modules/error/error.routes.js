(function () {
    'use strict';

    angular
        .module('app.error')
        .config(configRoutes);

    configRoutes.$inject = ['$stateProvider'];

    function configRoutes($stateProvider) {
        $stateProvider
            .state('error', {
                url: '/error',
                templateUrl: 'js/modules/error/error.tmpl.html',
                controller: 'ErrorController',
                controllerAs: 'vm'
            });
    }

})();