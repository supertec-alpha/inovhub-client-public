(function () {
    'use strict';

    angular
        .module('app.loading')
        .config(configRoutes);

    configRoutes.$inject = ['$stateProvider'];

    function configRoutes($stateProvider) {
        $stateProvider
            .state('loading', {
                url: '/loading',
                templateUrl: 'js/modules/loading/loading.tmpl.html',
                controller: 'LoadingController',
                controllerAs: 'vm'
            });
    }

})();