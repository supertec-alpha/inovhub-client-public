(function () {
    'use strict';

    angular
        .module('app.main')
        .controller('MainController', Main);

    Main.$inject = ['$rootScope', '$sce', '$timeout', '$state', 'SocketFactory', 'CoreService', '$scope'];

    function Main($rootScope, $sce, $timeout, $state, SocketFactory, CoreService, $scope) {
        /* jshint validthis:true */
        var vm = this;

        vm.trustHTML = trustHTML;
        vm.trustSrc = trustSrc;

        activate();

        SocketFactory.on('mac', function (mac) {
            CoreService.setMac(mac);
        });

        SocketFactory.on('settings', function (settings) {
            CoreService.setSettings(settings);
        });

        SocketFactory.on('platform', function (platform) {
            CoreService.setPlatform(platform);
        });

        SocketFactory.on('redirect', function (page) {
            $state.go(page);
        });

        SocketFactory.on('progress', function (state) {
            CoreService.setProgress(state);
        });

        SocketFactory.on('debug', function (debug) {
            CoreService.setDebug(debug);
        });

        SocketFactory.on('slots:send', function (slots) {
            CoreService.setSlots(slots);
        });

        SocketFactory.on('system:infos', function (infos) {
            CoreService.setSystemInfos(infos);
        });

        $scope.$watch(function () {
                return CoreService.getProgress();
            },
            function (progress) {
                $scope.progress = progress;
            }, true
        );

        $scope.$watch(function () {
                return CoreService.getDebug();
            },
            function (debug) {
                $scope.debug = debug;
            }, true
        );

        function activate() {}

        function trustHTML(html) {
            return $sce.trustAsHtml(html);
        }

        function trustSrc(src) {
            return $sce.trustAsResourceUrl(src);
        }
    }
})();