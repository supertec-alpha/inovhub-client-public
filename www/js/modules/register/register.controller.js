/*global angular*/

(function() {
    'use strict';

    angular
        .module('app.register')
        .controller('RegisterController', Register)

    Register.$inject = ['$scope', 'CoreService'];

    function Register($scope, CoreService, $timeout) {
        //var vm = this;

        $scope.mac = CoreService.getMac();
        $scope.$watch(function() {
                return CoreService.getSettings();
            },
            function(sett) {
                $scope.settings = sett;
            }, true
        );

        activate();

        function activate() {}
    }
})();