(function () {
    'use strict';

    angular
        .module('app.register')
        .config(configRoutes);

    configRoutes.$inject = ['$stateProvider'];

    function configRoutes($stateProvider) {
        $stateProvider
            .state('register', {
                url: '/register',
                templateUrl: 'js/modules/register/register.tmpl.html',
                controller: 'RegisterController',
                controllerAs: 'vm'
            });
    }

})();