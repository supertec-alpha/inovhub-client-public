/*global angular, $*/

(function() {
    'use strict';

    angular
        .module('app.sequence')
        .controller('SequenceController', Sequence)

    Sequence.$inject = ['$scope', '$timeout', '$interval', 'SocketFactory', 'CoreService'];

    function Sequence($scope, $timeout, $interval, SocketFactory, CoreService) {
        var vm = this;

        $scope.settings = {};
        $scope.$watch(function() {
                return CoreService.getSettings();
            },
            function(sett) {
                $scope.settings = sett;
            }, true
        );

        vm.slots = [];
        $scope.$watch(function() {
                return CoreService.getSlots();
            },
            function(slots) {
                slotsReceived(slots);
            }, true
        );

        $scope.systemInfos = {};
        $scope.$watch(function() {
                return CoreService.getSystemInfos();
            },
            function(infos) {
                $scope.systemInfos = infos;
            }, true
        );

        // Index du slot en cours dans le tableau des slots
        vm.currentSlotIndex = 0;

        // Gestionnaire du timeout de chaque slot
        var timeoutHandler;
        // Gestionnaire du minuteur
        var intervalHandler;

        var timerCurrent = 0;
        var timerMax = 0;

        activate();

        SocketFactory.on('video:ended', function() {
            if (vm.currentSlotIndex !== null) {
                if (vm.currentSlotIndex < vm.slots.length - 1) {
                    vm.currentSlotIndex++;
                } else {
                    vm.currentSlotIndex = 0;
                }
            } else {
                vm.currentSlotIndex = 0;
            }

            next();
        });

        SocketFactory.on('audio:ended', function() {
            if (vm.currentSlotIndex !== null) {
                if (vm.currentSlotIndex < vm.slots.length - 1) {
                    vm.currentSlotIndex++;
                } else {
                    vm.currentSlotIndex = 0;
                }
            } else {
                vm.currentSlotIndex = 0;
            }

            next();
        });

        SocketFactory.on('stream:ended', function() {
            if (vm.currentSlotIndex !== null) {
                if (vm.currentSlotIndex < vm.slots.length - 1) {
                    vm.currentSlotIndex++;
                } else {
                    vm.currentSlotIndex = 0;
                }
            } else {
                vm.currentSlotIndex = 0;
            }

            next();
        });

        function slotsReceived(newSlots) {
            let nbCurrentSlots = vm.slots.length;

            // on retire les templates vidéos
            if (Array.isArray(newSlots)) {
                newSlots.forEach(function(elem) {
                    // if the template has a filename ending in mp4 or it's a video template, it's handled in OMX so we can get rid of the layout HTML
                    if (elem.template.template_fields[0].slug == 'video' ||
                    elem.template.template_fields[0].slug == '__render') {
                        elem.layout.html = "<div></div>";
                    }
                });
            }

            if (!angular.equals(vm.slots, newSlots)) {
                vm.slots = newSlots;

                if (nbCurrentSlots <= 0 && vm.slots.length > 0) {
                    vm.currentSlotIndex = 0;
                    next();
                } else {
                    vm.currentSlotIndex = null;
                }
            }
        }

        function getExt(f) {
            if (!f) return null;
            return f.split('.').pop().toLowerCase();
        }

        function next() {
            if (typeof vm.slots !== 'undefined' && vm.slots.length > 0) {
                timerCurrent = 0;
                timerMax = vm.slots[vm.currentSlotIndex].duration;

                //console.log(Math.round(timerCurrent / timerMax * 100) + "%");

                // On supprime le timeout
                $timeout.cancel(timeoutHandler);

                // On clean tout
                /*$('video').each(function(i, vdo) {
                    console.log(vdo);
                    vdo.pause();
                    vdo.src = '';
                    vdo.children('source').prop('src', '');
                    vdo.remove().length = 0;
                });*/

                // On attribue le slot en cours
                if (CoreService.getPlatform() == 'linux') {
                    if (vm.slots[vm.currentSlotIndex].template.template_fields[0].slug == "stream" &&
                        (vm.slots[vm.currentSlotIndex].template.template_fields[0].value.startsWith("rtmp://") ||
                        vm.slots[vm.currentSlotIndex].template.template_fields[0].value.startsWith("rtsp://"))) {
                        SocketFactory.emit('stream:play', vm.slots[vm.currentSlotIndex].template.template_fields[0].value);
                    } else if (getExt(vm.slots[vm.currentSlotIndex].template.template_fields[0].filename) == 'mp4' ) {
                        SocketFactory.emit('video:play', vm.slots[vm.currentSlotIndex].template.template_fields[0].filename);
                    } else if (vm.slots[vm.currentSlotIndex].template.template_fields[0].slug == "__render" &&
                        vm.slots[vm.currentSlotIndex].template.template_fields[0].value &&
                        getExt(vm.slots[vm.currentSlotIndex].template.template_fields[0].value
                        .slice(vm.slots[vm.currentSlotIndex].template.template_fields[0].value.lastIndexOf('/') + 1,
                        vm.slots[vm.currentSlotIndex].template.template_fields[0].value.lastIndexOf('?decache'))) == 'mp4'){
                            // this is a video template, play it with OMX
                            var videoName = vm.slots[vm.currentSlotIndex].template.template_fields[0].value
                                .slice(vm.slots[vm.currentSlotIndex].template.template_fields[0].value.lastIndexOf('/') + 1
                                ,vm.slots[vm.currentSlotIndex].template.template_fields[0].value.lastIndexOf('?decache'));
                            //console.log(videoName);
                            SocketFactory.emit('video:play', videoName);
                    } else if (vm.slots[vm.currentSlotIndex].template.template_fields[0].slug == "audio") /*&&
                    vm.slots[vm.currentSlotIndex].template.template_fields[0].value &&
                    vm.slots[vm.currentSlotIndex].template.template_fields[0].value.lastIndexOf('?decache') == 'mp3')*/{
                        // this is a video template, play it with OMX
                        SocketFactory.emit('audio:play', vm.slots[vm.currentSlotIndex].template.template_fields[0].filename);
                } else {
                        // Timeout gérant la durée du slot
                        timeoutHandler = $timeout(function() {

                            // On annule l'interval
                            //$interval.cancel(intervalHandler);

                            //console.log("100%");

                            if (vm.currentSlotIndex !== null) {
                                if (vm.currentSlotIndex < vm.slots.length - 1) {
                                    vm.currentSlotIndex++;
                                } else {
                                    vm.currentSlotIndex = 0;
                                }
                            } else {
                                vm.currentSlotIndex = 0;
                            }

                            next();
                        }, vm.slots[vm.currentSlotIndex].duration);
                    }
                } else {
                    // Timeout gérant la durée du slot
                    timeoutHandler = $timeout(function() {

                        // On annule l'interval
                        //$interval.cancel(intervalHandler);

                        //console.log("100%");
                        if (vm.currentSlotIndex !== null) {
                            if (vm.currentSlotIndex < vm.slots.length - 1) {
                                vm.currentSlotIndex++;
                            } else {
                                vm.currentSlotIndex = 0;
                            }
                        } else {
                            vm.currentSlotIndex = 0;
                        }

                        next();
                    }, vm.slots[vm.currentSlotIndex].duration);
                }

                // Interval gérant l'affichage de la progression du slot en cours
                /* intervalHandler = $interval(function() {
                    timerCurrent += 1000;
                    //console.log(Math.round(timerCurrent / timerMax * 100) + "%");
                }, 1000); */
            }
        }

        function activate() {
            // on réclame les slots
            SocketFactory.emit('ready');
            //console.log("slots:get");
        }

        $scope.$on('$destroy', function() {
            $timeout.cancel(timeoutHandler);
            //$interval.cancel(intervalHandler);
        });
    }
})();