(function () {
    'use strict';

    angular
        .module('app.sequence')
        .config(configRoutes);

    configRoutes.$inject = ['$stateProvider'];

    function configRoutes($stateProvider) {
        $stateProvider
            .state('sequence', {
                url: '/sequence',
                templateUrl: 'js/modules/sequence/sequence.tmpl.html',
                controller: 'SequenceController',
                controllerAs: 'sequenceCtrl'
            });
    }

})();