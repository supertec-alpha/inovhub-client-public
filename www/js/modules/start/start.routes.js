(function () {
    'use strict';

    angular
        .module('app.start')
        .config(configRoutes);

    configRoutes.$inject = ['$stateProvider'];

    function configRoutes($stateProvider) {
        $stateProvider
            .state('start', {
                url: '/',
                templateUrl: 'js/modules/start/start.tmpl.html',
                controller: 'StartController',
                controllerAs: 'vm'
            });
    }

})();