/*global angular*/

(function() {
    'use strict';

    angular
        .module('app.wifi')
        .controller('WifiController', Wifi)

    Wifi.$inject = ['CoreService', '$scope', '$timeout'];

    function Wifi(CoreService, $scope, $timeout) {
        var vm = this;

        $scope.settings = {};

        $scope.mac = CoreService.getMac();
        $scope.$watch(function() {
                return CoreService.getSettings();
            },
            function(sett) {
                $scope.settings = sett;
            }, true
        );

        activate();

        function activate() {}
    }
})();